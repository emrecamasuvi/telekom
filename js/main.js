// ready go
// jquery start
$(function(){

	// define map is a must
	var map;
	// global marker image with logo
	var markerimage = new google.maps.MarkerImage('http://i.imgur.com/t68M151.png',
		new google.maps.Size(30, 43),
		new google.maps.Point(0,0),
		new google.maps.Point(0, 0)
	);

	// call tooltip
	$(".tooltipy").tooltip();

	// tabs
	$("#tabs").on("click", "a", function(){
		var tabhref = $(this).data("href");
		var tabsli = $("#tabs").find("li");
		var tabli = $("#tabs_href").find("li");
		var tabtarget= tabli.filter(function() { 
		  return $(this).data("toggle") == tabhref
		});
		if(!$(".butsmaller").length){
			$(".howtouse").addClass("butsmaller");
		}
		tabsli.removeClass("active");
		$(this).parent().addClass("active");
		tabli.hide();
		tabtarget.slideDown(function(){

			// call map for karte tab
			if (tabhref=='karte'){
				Karte();
		    google.maps.event.trigger(map, "resize");
			}

			// call map for route tab
			if (tabhref=='route'){
				Route();
		    google.maps.event.trigger(map, "resize");
			}

		});

	});

	/*
	i've used gmaps.js u can read docs at:
	http://hpneo.github.com/gmaps/examples.html
	===================================
	======== TO ENCODE ADDRESS ========
	===================================
	GMaps.geocode({
	  address: $('#address').val(),
	  callback: function(results, status) {
	    if (status == 'OK') {
	      var latlng = results[0].geometry.location;
	      map.setCenter(latlng.lat(), latlng.lng());
	      map.addMarker({
	        lat: latlng.lat(),
	        lng: latlng.lng()
	      });
	    }
	  }
	});

	===================================
	 ==== TO READ CURRENT LOCATION ====
	===================================
	GMaps.geolocate({
    success: function(position){
      map.setCenter(position.coords.latitude, position.coords.longitude);
    },
    error: function(error){
      alert('Geolocation failed: '+error.message);
    },
    not_supported: function(){
      alert("Your browser does not support geolocation");
    },
    always: function(){
      alert("Done!");
    }
  });
	*/

	var Karte = function Karte(){
		map = new GMaps({
      div: '#mapcarte',
      lat: -12.043333,
      lng: -77.028333
    });
    map.addMarker({
      lat: -12.043333,
      lng: -77.03,
      title: "Map's title",
      icon: markerimage,
      details: {
        database_id: 123456,
        author: 'dbauthor'
      },
      infoWindow: {
        content: '<div class="PTsans addresspopup">'
										+'<h3 class="bolder">Türk Telekom Mobile Partner Shop Köln 15</h3>'
										+'<p>Dürener Str. 138 / 50931 Köln</p>'
										+'<h3 class="bolder">Öffnungszeiten:</h3>'
										+'<p>Mo. Di. Mi. Do. Fri.: 10:00 - 19:00 Sa.: 10:00 - 15:00</p>'
										+'<h3 class="bolder">Kontakt:</h3>'
										+'<p>Fon: 0221 16917522</p>'
										+'<p>Fax: 0221 16917522</p>'
										+'<p>Zusatzliche Sprachen: Englisch, Türkisch</p>'
										+'<a href="#rota" class="popuplink">Route berechen</a>'
        					+'</div>'
      }
    });
	}

	var Route = function Route(){
	// define view coordinates
	  map = new GMaps({
	    div: '#maproute',
	    lat: -12.043333,
	    lng: -77.028333
	  });
	  // define start point in map
	  map.addMarker({
	    lat: -12.044012922866312,
	    lng: -77.02470665341184,
	    title: "current location",
	    icon: markerimage
	  });
	  // define target point in map
	  map.addMarker({
	    lat: -12.090814532191756,
	    lng: -77.02271108990476,
	    title: "the target",
	    icon: markerimage
	  });

	  //  router description
	  $('#travel').click(function(e){
      e.preventDefault();
      $("#instructions").empty();
      map.travelRoute({
        origin: [-12.044012922866312, -77.02470665341184],
        destination: [-12.090814532191756, -77.02271108990476],
        travelMode: 'driving',
        step: function(e){
          $('#instructions').append('<li>'+e.instructions+'</li>');
          $('#instructions li:eq('+e.step_number+')').delay(450*e.step_number).fadeIn(200, function(){
            map.setCenter(e.end_location.lat(), e.end_location.lng());
            map.drawPolyline({
              path: e.path,
              strokeColor: '#2d8ce4',
              strokeOpacity: 0.7,
              strokeWeight: 4
            });
          });
        }
      });
	  });
	}

	// toggle route secription or map view for radio buttons in route tab
	$(".form-filter-by").on("change", "input[name='togglemaps']", function(){
		$("#instructions").toggleClass("sticktomap");
	});

	// print
	$(".print").on("click", function(){
		window.print();
	});

	// search again resets form
	$(".searchagain").on("click", function(e){
		e.preventDefault();
		$("#mainform, #tabs, #tabs_href").slideToggle(function(){
			$(".howtouse").removeClass("butsmaller");
		});
		// further form data erasing options here please

	});

});